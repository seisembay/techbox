//
//  Component.swift
//  Shoebox
//
//  Created by Islam Seisembay on 26/03/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

struct Component {
    var name: String
    var image: UIImage?
    var used: Bool
    var quantity: Int
}

extension Component: Equatable {
    static func == (lhs: Component, rhs: Component) -> Bool {
        return lhs.name == rhs.name
    }
}
