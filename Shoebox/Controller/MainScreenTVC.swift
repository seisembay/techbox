//
//  MainScreenTVC.swift
//  Shoebox
//
//  Created by Islam Seisembay on 26/03/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

protocol Sensor {
    func send(model: Component)
}

class MainScreenTVC: UITableViewController {
    
    var components: [Component] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font : UIFont(name: "SteradianBold", size: 32)]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont(name: "SteradianBold", size: 16)]

        view.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        components = getData() != nil ? getData()! : getSampleData()

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    
    func getData() -> [Component]? {
        return nil
    }
    
    func getSampleData() -> [Component] {
        return [Component(name: "LCD Sensor", image: nil, used: false, quantity: 2),
                Component(name: "Temperature Sensor", image: nil, used: false, quantity: 4),
                Component(name: "Button", image: nil, used: false, quantity: 5),
                Component(name: "Potentiometer", image: nil, used: false, quantity: 3)]
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return components.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComponentCell", for: indexPath) as! ComponentTVCell
        
        cell.awakeFromNib()
        
        cell.sensorImage.image = UIImage(named: "\(components[indexPath.row].name)")
        cell.componentTitle.text = components[indexPath.row].name
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height/8
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "toVC" else { return }
        if let vc = segue.destination as? ViewController {
            vc.delegatue = self
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainScreenTVC: Sensor {
    func send(model: Component) {
        if !components.contains(model) {
            self.components.insert(model, at: 0)
        } else {
            if let index = components.firstIndex(of: model) {
                components[index] = Component(name: model.name, image: model.image, used: model.used, quantity: model.quantity + 1)
            }
        }
        
        self.tableView.reloadData()
    }
}
