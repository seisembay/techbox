//
//  ViewController.swift
//  Shoebox
//
//  Created by Islam Seisembay on 25/03/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit
import AVKit
import Vision

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var identifiedSensorTitle: UILabel!
    @IBOutlet weak var bgViewForConfidence: UIView!
    @IBOutlet weak var learnMoreBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var errorInScanBtn: UIButton!
    @IBOutlet weak var notifierView: UIView!
    @IBOutlet weak var notifierTextLbl: UILabel!
    
    var model: Component?
    var modelName: String?
    
    var delegatue: Sensor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateViews()
        
        let captureSession = AVCaptureSession()
        
        guard let captureDevice = AVCaptureDevice.default(for: .video) else { return }
        
        guard let input = try? AVCaptureDeviceInput(device: captureDevice) else { return }
        
        captureSession.addInput(input)
        captureSession.startRunning()
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = CGRect(x: 2, y: 0, width: cameraView.frame.width-8, height: cameraView.frame.height-5)
        previewLayer.cornerRadius = 10
        previewLayer.masksToBounds = true
        cameraView.layer.addSublayer(previewLayer)
        
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        captureSession.addOutput(dataOutput)
        
        // Do any additional setup after loading the view.
    }
    
    func updateViews() {
        bgViewForConfidence.layer.cornerRadius = 32
        bgViewForConfidence.heightAnchor.constraint(equalToConstant: self.view.frame.height/4 + 20).isActive = true
        
        confidenceLabel.setFontToSteradian(.Medium, size: 12, color: UIColor(white: 0, alpha: 0.4))
        identifiedSensorTitle.setFontToSteradian(.Bold, size: 24, color: UIColor(white: 0, alpha: 0.7))
        
        learnMoreBtn.setupBtn(color: UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1))
        learnMoreBtn.heightAnchor.constraint(equalToConstant: bgViewForConfidence.frame.height/3).isActive = true
        learnMoreBtn.setTitle("Learn more", for: .normal)
        learnMoreBtn.setFontToSteradian(.Medium, size: 16, color: .white)
        
        addBtn.setupBtn(color: UIColor(red: 121/255, green: 183/255, blue: 249/255, alpha: 1))
        addBtn.heightAnchor.constraint(equalToConstant: bgViewForConfidence.frame.height/3).isActive = true
        addBtn.setTitle("Add to storage", for: .normal)
        addBtn.setFontToSteradian(.Medium, size: 16, color: .white)
        
        errorInScanBtn.setTitle("Not what you are looking for?", for: .normal)
        errorInScanBtn.setFontToSteradian(.Regular, size: 10, color: .gray)
        
        notifierView.alpha = 0
        notifierView.layer.cornerRadius = notifierView.frame.height/2 - 5
        notifierTextLbl.text = ""
        
        
    }

    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        print("camera captured a frame output at:", Date())
        
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        guard let model = try? VNCoreMLModel(for: SensorClassifier_1().model) else { return }
        let request = VNCoreMLRequest(model: model) { (finishedReq, err) in
            // print(finishedReq.results)
            
            guard let results = finishedReq.results as? [VNClassificationObservation] else { return }
            guard let firstObservation = results.first else { return }
            
            self.modelName = firstObservation.identifier
            
            DispatchQueue.main.async {
                self.identifiedSensorTitle.text = firstObservation.identifier
                self.confidenceLabel.text = "\(firstObservation.confidence)"
            }
            //print(firstObservation.identifier, firstObservation.confidence)
        }
        
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
    }
    
    @IBAction func addBtnTapped(_ sender: Any) {
        model = Component(name: modelName!, image: UIImage(named: "\(modelName)"), used: false, quantity: 1)
        delegatue?.send(model: model!)
        notifierTextLbl.text = "\(modelName!) added"
        notifierTextLbl.setFontToSteradian(.Medium, size: 12, color: .white)
        sensorAddedNotif()
        print("Added: \(modelName!)")
    }
    
    func sensorAddedNotif() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                self.notifierView.alpha = 1
                
            }) { (_) in
                UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseInOut, animations: {
                    self.notifierView.alpha = 0
                    
                }, completion: nil)
            }
        }
        
        
    }
    
}

