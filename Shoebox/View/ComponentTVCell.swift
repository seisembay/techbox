//
//  ComponentTVCell.swift
//  Shoebox
//
//  Created by Islam Seisembay on 26/03/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

class ComponentTVCell: UITableViewCell {

    @IBOutlet weak var bgShadowView: UIView!
    @IBOutlet weak var componentTitle: UILabel!
    @IBOutlet weak var sensorImage: UIImageView!
    
    var colour = UIColor.black
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
        
        self.backgroundColor = .clear
        bgShadowView.backgroundColor = .clear
        //bgShadowView.heightAnchor.constraint(equalToConstant: self.frame.height - 20).isActive = true
        self.layer.masksToBounds = false
        
        bgShadowView.configureShad(radius: bgShadowView.frame.size.height/2 - 4, size: bgShadowView.frame.size, color: colour)
        bgShadowView.layer.masksToBounds = false
        print(bgShadowView.frame.origin)
        componentTitle.setFontToSteradian(.Medium, size: 12, color: UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1))
        
        sensorImage.heightAnchor.constraint(equalToConstant: bgShadowView.frame.height).isActive = true
        sensorImage.widthAnchor.constraint(equalToConstant: bgShadowView.frame.height).isActive = true
        
        if sensorImage.image == nil {
            sensorImage.image = UIImage(named: "noimage")
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
