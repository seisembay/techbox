//
//  Font Setup.swift
//  Shoebox
//
//  Created by Islam Seisembay on 01/04/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

enum FontWeight {
    case Light,Regular, Medium, Bold
}

extension UILabel {
    
    //Set to font
    func setFontToSteradian(_ weight: FontWeight, size: CGFloat, color: UIColor) {
        switch weight {
        case .Light:
            self.font = UIFont(name: "SteradianLight", size: size)
        case .Regular:
            self.font = UIFont(name: "SteradianRegular", size: size)
        case .Medium:
            self.font = UIFont(name: "SteradianMedium", size: size)
        case .Bold:
            self.font = UIFont(name: "SteradianBold", size: size)
        }
        self.textColor = color
    }
    
}

extension UITextField {
    //Set to font
    func setFontToSteradian(_ weight: FontWeight, size: CGFloat, color: UIColor) {
        switch weight {
        case .Light:
            self.font = UIFont(name: "SteradianLight", size: size)
        case .Regular:
            self.font = UIFont(name: "SteradianRegular", size: size)
        case .Medium:
            self.font = UIFont(name: "SteradianMedium", size: size)
        case .Bold:
            self.font = UIFont(name: "SteradianBold", size: size)
        }
        self.textColor = color
    }
}

extension UITextView {
    func setFontToSteradian(_ weight: FontWeight, size: CGFloat, color: UIColor) {
        switch weight {
        case .Light:
            self.font = UIFont(name: "SteradianLight", size: size)
        case .Regular:
            self.font = UIFont(name: "SteradianRegular", size: size)
        case .Medium:
            self.font = UIFont(name: "SteradianMedium", size: size)
        case .Bold:
            self.font = UIFont(name: "SteradianBold", size: size)
        }
        self.textColor = color
    }
}

extension UIButton {
    func setFontToSteradian(_ weight: FontWeight, size: CGFloat, color: UIColor) {
        switch weight {
        case .Light:
            self.titleLabel?.font = UIFont(name: "SteradianLight", size: size)!
        case .Regular:
            self.titleLabel?.font = UIFont(name: "SteradianRegular", size: size)!
        case .Medium:
            self.titleLabel?.font = UIFont(name: "SteradianMedium", size: size)!
        case .Bold:
            self.titleLabel?.font = UIFont(name: "SteradianBold", size: size)!
        }
        self.titleLabel?.textColor = color
    }
}
