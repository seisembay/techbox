//
//  Extension.swift
//  Shoebox
//
//  Created by Islam Seisembay on 01/04/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

extension UIView {
    
    func bgForTextField() {
        self.layer.cornerRadius = 24
    }
    
    func configureShad(radius: CGFloat, size: CGSize, color: UIColor) {
        let l = CALayer()
        l.name = "layer1"
        let l2 = CALayer()
        l2.name = "layer2"
        
        l.frame.origin = CGPoint(x: 0, y: 0)
        l.frame.size = size
        l.cornerRadius = radius - 5
        l2.frame.origin = CGPoint(x: 0, y: 0)
        l2.frame.size = size
        l2.cornerRadius = radius - 5
        
        l.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        l2.backgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1).cgColor
        
        l.shadowColor = color.cgColor
        l.shadowOffset = CGSize(width: 3, height: 3)
        l.shadowRadius = 3
        l.shadowOpacity = color != UIColor.black ? 0.3 : 0.05
        l.masksToBounds = false
        
        l2.shadowColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1).cgColor
        l2.shadowOffset = CGSize(width: -3, height: -3)
        l2.shadowRadius = 3
        l2.shadowOpacity = 0.3
        l2.masksToBounds = false
        
        replaceLayer(with: l)
        replaceLayer(with: l2)
        //self.layer.addSublayer(l)
        //self.layer.addSublayer(l2)
        self.layer.masksToBounds = false
    }
    
    
    func replaceLayer(with layerr: CALayer) {
        _ = self.layer.sublayers?.map {
            if $0.name == layerr.name {
                $0.removeFromSuperlayer()
            }
        }
        self.layer.addSublayer(layerr)
    }
    
    func setupBtn(color: UIColor) {
        self.layer.cornerRadius = self.frame.height/2 - 5
        self.backgroundColor = color
        
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.8
        self.layer.masksToBounds = false
    }
    
}
